CREATE TABLE tdcuotasffmm (
     NUMERO varchar,
     RUT_ADMINISTRADORA varchar,
     RAZON_SOCIAL_ADMINISTRADORA varchar,
     RUN_FONDO varchar,
     NOMBRE_FONDO varchar,
     NOMBRE_CORTO varchar,
     TIPO_FONDO varchar,
     FECHA_CARTOLA varchar,
     ACTIVO_TOTAL varchar,
     MONEDA varchar,
     PARTICIPES_INSTITUCIONALES varchar,
     INVERSION_FONDOS decimal,
     SERIE varchar,
     CUOTAS_APORTADAS decimal,
     CUOTAS_RESCATADAS decimal,
     CUOTAS_CIRCULACION decimal,
     VALOR_CUOTA decimal,
     PATRIMONIO_NETO decimal,
     NUMERO_PARTICIPES decimal,
     NUMERO_PARTICIPES_INST decimal,
     FONDO_PEN varchar,
     REM_FIJA_SA decimal,
     REM_VARIABLE_SA decimal,
     GASTOS_AFECTOS decimal,
     GASTOS_NO_AFECTOS decimal,
     COMISION_INVERSION decimal,
     COMISION_RESCATE decimal,
     FACTOR_AJUSTE decimal null,
     FACTOR_REPARTO decimal NULL,
     fecha_datos date,
     fecha_proceso date 
)
;
CREATE TABLE tdffmmrentabilidad
(
fecha_datos date ,
run_serie varchar,
periodo varchar ,
retorno float
)