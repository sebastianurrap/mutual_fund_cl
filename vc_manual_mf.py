from library_mutual_fund import *
from datetime import datetime, timedelta
import pandas as pd
import time

path='./input/ffmm_todos_20220201_20220228.txt'

start_date=path[19:27]
end_date=path[28:36]


start = time.time()
tdcargatdvcffmm_manual(path,start_date,end_date)
end = time.time()
print(end - start," se cargó exitosamente los vc de ",start_date," a ",end_date)