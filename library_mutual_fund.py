from download_api_data import *
import sqlalchemy
from sqlalchemy import create_engine
from datetime import datetime, timedelta
import pandas as pd
import dateutil.relativedelta


def tdcargatdvcffmm(process_date):

    engine_pascal = sqlalchemy.create_engine("postgresql+psycopg2://postgres:qwerty123@dbfund.csnfyczvut0m.us-east-1.rds.amazonaws.com/postgres", echo=False)
    conn = engine_pascal.raw_connection()

    df_get_df_daily_mutual_funds_statement=get_df_daily_mutual_funds_statement(process_date).reset_index(drop=True)

    df_get_df_daily_mutual_funds_statement=df_get_df_daily_mutual_funds_statement.rename(columns={
    "NUMERO":"numero",
    "RUT_ADMINISTRADORA":"rut_administradora",
    "RAZON_SOCIAL_ADMINISTRADORA":"razon_social_administradora",
    "RUN_FONDO":"run_fondo",
    "NOMBRE_FONDO":"nombre_fondo",
    "NOMBRE_CORTO":"nombre_corto",
    "TIPO_FONDO":"tipo_fondo",
    "FECHA_CARTOLA":"fecha_cartola",
    "ACTIVO_TOTAL":"activo_total",
    "MONEDA":"moneda",
    "PARTICIPES_INSTITUCIONALES":"participes_institucionales",
    "INVERSION_FONDOS":"inversion_fondos",
    "SERIE":"serie",
    "CUOTAS_APORTADAS":"cuotas_aportadas",
    "CUOTAS_RESCATADAS":"cuotas_rescatadas",
    "CUOTAS_CIRCULACION":"cuotas_circulacion",
    "VALOR_CUOTA":"valor_cuota",
    "PATRIMONIO_NETO":"patrimonio_neto",
    "NUMERO_PARTICIPES":"numero_participes",
    "NUMERO_PARTICIPES_INST":"numero_participes_inst",
    "FONDO_PEN":"fondo_pen",
    "REM_FIJA_SA":"rem_fija_sa",
    "REM_VARIABLE_SA":"rem_variable_sa",
    "GASTOS_AFECTOS":"gastos_afectos",
    "GASTOS_NO_AFECTOS":"gastos_no_afectos",
    "COMISION_INVERSION":"comision_inversion",
    "COMISION_RESCATE":"comision_rescate",
    "FACTOR_AJUSTE":"factor_ajuste",
    "FACTOR_REPARTO":"factor_reparto"})

    df_get_df_daily_mutual_funds_statement['fecha_datos']=datetime.strptime(process_date,'%Y%m%d')
    df_get_df_daily_mutual_funds_statement['fecha_proceso']=datetime.today()

    elimina_datos=f'''DELETE FROM tdvcffmm WHERE fecha_datos='{process_date}' '''
    cursor = conn.cursor()
    cursor.execute(elimina_datos)
    conn.commit()

    df_get_df_daily_mutual_funds_statement.to_sql('tdvcffmm', engine_pascal,index=False, if_exists='append',index_label=False)
    conn.close()


def tdcargatdvcffmm_manual(path,start_date,end_date):
    df = pd.read_csv(path, sep = ";")

    engine_pascal = sqlalchemy.create_engine("postgresql+psycopg2://postgres:qwerty123@dbfund.csnfyczvut0m.us-east-1.rds.amazonaws.com/postgres", echo=False)
    conn = engine_pascal.raw_connection()
    df=df.rename(columns={'RUN_ADM':"rut_administradora",
        'NOM_ADM':"razon_social_administradora",
        'RUN_FM':"run_fondo",
        'FECHA_INF':"fecha_cartola",
        'ACTIVO_TOT':"activo_total",
        'MONEDA':"moneda",
        'PARTICIPES_INST':"participes_institucionales",
        'INVERSION_EN_FONDOS':"inversion_fondos",
        'SERIE':"serie",
        'CUOTAS_APORTADAS':"cuotas_aportadas",
        'CUOTAS_RESCATADAS':"cuotas_rescatadas",
        'CUOTAS_EN_CIRCULACION':"cuotas_circulacion",
        'VALOR_CUOTA':"valor_cuota",
        'PATRIMONIO_NETO':"patrimonio_neto",
        'NUM_PARTICIPES':"numero_participes",
        'NUM_PARTICIPES_INST':"numero_participes_inst",
        'FONDO_PEN':"fondo_pen",
        'REM_FIJA':"rem_fija_sa",
        'REM_VARIABLE':"rem_variable_sa",
        'GASTOS_AFECTOS':"gastos_afectos",
        'GASTOS_NO_AFECTOS':"gastos_no_afectos",
        'COMISION_INVERSION':"comision_inversion",
        'COMISION_RESCATE':"comision_rescate",
        'FACTOR DE AJUSTE':"factor_ajuste",
        'FACTOR DE REPARTO':"factor_reparto"})
    df['nombre_fondo']=''
    df['nombre_corto']=''
    df['tipo_fondo']=''
    df['numero']=df.index
    df['numero']=df['numero'].astype(int)
    df['numero']=df['numero']+1
    df['fecha_datos']=df['fecha_cartola'].apply(transform_date)
    df['fecha_proceso']=datetime.today()

    elimina_datos=f'''DELETE FROM tdvcffmm WHERE fecha_datos between '{start_date}' and '{end_date}' '''
    cursor = conn.cursor()
    cursor.execute(elimina_datos)
    conn.commit()

    df.to_sql('tdvcffmm', engine_pascal,index=False, if_exists='append',index_label=False)
    conn.close()

def transform_date(x):
    x=str(x)
    return datetime.strptime(x,'%Y%m%d')

def day_before(dt,year,month,day): 
    dt=dt-dateutil.relativedelta.relativedelta(years=year)
    dt=dt-dateutil.relativedelta.relativedelta(months=month)
    dt=dt-dateutil.relativedelta.relativedelta(days=day)
    return dt

def get_df_vc(date,cnn):
    """
    Devuelve la data de valor cuota de todos los fondos mutuos
    param process_date: string con la fecha en formato 'YYYYMMDD'
    param cnn: conexion a base de datos
    """
#--and AL1.fecha = dateadd(day, -datepart(day,dateadd(month,1, AL1.fecha)), dateadd(month,1, AL1.fecha))
    df_data = pd.read_sql(f'''
                    select * from tdvcffmm 
            ''', con = cnn)
    return df_data
def return_day_before(dt):
    dt=dt-dateutil.relativedelta.relativedelta(day=1)
    return dt

def return_moneda(x):
    if x=='$$':
        return 'CLP'
    elif x=='PROM':
        return 'USD'
    else:
        return x