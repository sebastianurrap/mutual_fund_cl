from library_mutual_fund import *
from datetime import datetime, timedelta
import time

#fecha de ayer transformada a string 
today=datetime.now()
yesterday=today-timedelta(days=1)
process_date=yesterday.strftime('%Y%m%d')
day_number=datetime.today().weekday()+1

if day_number in (2,3,4,5):
    try:
        start = time.time()
        tdcargatdvcffmm(process_date)
        end = time.time()
        print("Se cargó exitosamente los valores cuota en tdvcffmm para el día: ", yesterday, "en ", end - start)
        end = time.time()
    except:
        print("No se cargó los valores cuota en tdvcffmm para el día: ", yesterday)
elif day_number==1:
    start_day=today-timedelta(days=4)
    day_count = (yesterday - start_day).days + 1 
    for single_date in [d for d in (start_day + timedelta(n) for n in range(day_count)) if d <= yesterday]:
        start = time.time()
        single_date=datetime.strftime(single_date, '%Y%m%d')
        time.sleep(15)
        try:
            tdcargatdvcffmm(single_date)
            print("Se cargó exitosamente los valores cuota en tdvcffmm para el día: ", single_date)
        except:
            try:
                time.sleep(10)
                tdcargatdvcffmm(single_date)
                print("Se cargó exitosamente los valores cuota en tdvcffmm para el día: ", single_date)
            except:
                print("Hubo un problema con la descarga para el dia ", single_date)
        end = time.time()
        print("tiempo: ",end - start)
        