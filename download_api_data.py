import pandas as pd
import json
from http.client import HTTPSConnection
from base64 import b64encode

USER = 'USR_LVA_1'
PASS = 'TvzK43_m.V'
BASE_URL = 'www.cmfchile.cl'
client = HTTPSConnection(BASE_URL)
headers = {"Authorization": "Basic {}".format(b64encode(bytes(f"{USER}:{PASS}", "utf-8")).decode("ascii"))}

# FMCFM: Cartola diaria de Fondos Mutuos
def get_df_daily_mutual_funds_statement(process_date):
    '''
    param process_date: String de la forma 'yyyymmdd' que permite obtener la cartola diaria de fondos mutuos
    Esta funcion devuelve un dataframe que contiene:

    |NUMERO|RUT_ADMINISTRADORA|RAZON_SOCIAL_ADMINISTRADORA|RUN_FONDO|NOMBRE_FONDO|NOMBRE_CORTO|TIPO_FONDO|FECHA_CARTOLA|ACTIVO_TOTAL|MONEDA|
    PARTICIPES_INSTITUCIONALES|INVERSION_FONDOS|SERIE|CUOTAS_APORTADAS|CUOTAS_RESCATADAS|CUOTAS_CIRCULACION|VALOR_CUOTA|PATRIMONIO_NETO|
    NUMERO_PARTICIPES|NUMERO_PARTICIPES_INST|FONDO_PEN|REM_FIJA_SA|REM_VARIABLE_SA|GASTOS_AFECTOS|GASTOS_NO_AFECTOS|COMISION_INVERSION|
    COMISION_RESCATE|FACTOR_AJUSTE|FACTOR_REPARTO|
    '''
    API_URL = f'/sitio/api/fmcfm/consulta_cartola/{process_date}/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_daily_mutual_funds_statement = pd.DataFrame(json_data['Data'])
        return df_daily_mutual_funds_statement
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api para la fecha {process_date}, volviendo a intentar')
        get_df_daily_mutual_funds_statement(process_date)

#FICFI: Cartola diaria de Fondos de Inversión
def get_df_daily_investment_funds_poster(process_date):
    
    '''
    param process_date: String de la forma 'yyymmdd' que permite obtener la cartola diaria de los valores cuota de los fondos de inversion.
    Esta funcion devuelve un dataframe con los siguientes datos:

    NUMERO|ID|RUT_ADMINISTRADORA|RAZON_SOCIAL_ADM|RUN_FONDO|NOMBRE_FONDO|NOMBRE_CORTO|RESCATABLE|FECHA_CARTOLA|CUOTAS_EMITIDAS|CUOTAS_PAGADAS|VALOR_CUOTA|
    VALOR_ECONOMICO|PATRIMONIO_NETO|ACTIVO_TOTAL|NUMERO_APORTANTES|NUMERO_APORTANTES_INST|MONEDA|AGENCIA|SERIE
    '''
    
    API_URL = f'/sitio/api/ficfi/consulta_cartola/{process_date}/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_daily_investment_funds_poster = pd.DataFrame(json_data['Data'])
        return df_daily_investment_funds_poster
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api para la fecha {process_date}, volviendo a intentar')
        get_df_daily_investment_funds_poster(process_date)

#FMIDE: Identificación de los Fondos Mutuos
def get_df_mutual_funds_identification():
    '''
    Esta funcion devuelve un dataframe con la informacion de todos los fondos mutuos:

    |NUMERO|RUN_FONDO|DV_FONDO|NOMBRE_FONDO|NOMBRE_CORTO|VIGENCIA|TIPO_FONDO|MONEDA|RUT_ADMINISTRADORA|DV_ADMINISTRADORA|NOMBRE_ADMINISTRADORA|
    FECHA_DEPOSITO|FECHA_ULT_MODIF|FEC_INICIO_OPER|FEC_TERMINO_OPER|FEC_CUMPLIMIENTO|NRO_FEC_RES_APROB|NUMERO_REGISTRO|
    '''
    API_URL = f'/sitio/api/fmide/identificacion/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_mutual_funds_identification = pd.DataFrame(json_data['Data'])
        return df_mutual_funds_identification
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api, volviendo a intentar')
        get_df_mutual_funds_identification()

#FIIDE: Identificación de los Fondos de Inversión
def get_df_investment_funds_identification():

    '''
    Esta funcion devuelve un dataframe con los siguientes datos de los fondos de inverion:

    NUMERO|RUN_FONDO|DV_FONDO|NOMBRE_FONDO|NOMBRE_CORTO|VIGENCIA|RESCATABLE|MONEDA|RUT_ADMINISTRADORA|DV_ADMINISTRADORA|NOMBRE_ADMINISTRADORA|
    FEC_RES_APROB|FEC_INICIO_OPER|FEC_TERMINO_OPER

    '''
    API_URL = f'/sitio/api/fmide/identificacion/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_investment_funds_identification = pd.DataFrame(json_data['Data'])
        return df_investment_funds_identification
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api, volviendo a intentar')
        get_df_investment_funds_identification()

#FOSER: Series de los Fondos
def get_df_funds_series():

    '''
    Esta funcion retorna un dataframe con los siguientes datos de las series de los fondos:

    NUMERO|RUN_FONDO|DV_FONDO|CLASE_FONDO|NOMBRE_FONDO|NOMBRE_CORTO|RUT_ADMINISTRADORA|DV_ADMINISTRADORA|NOMBRE_ADMINISTRADORA|RESCATABLE|SERIE|
    FEC_INI_SERIE|FEC_FIN_SERIE|VAL_CUO_INI|CONTINUADORA_DE_SERIE|CARACTERISTICA|NEMOTECNICO

    '''
    API_URL = f'/sitio/api/foser/series/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_funds_series = pd.DataFrame(json_data['Data'])
        return df_funds_series
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api, volviendo a intentar')
        get_df_funds_series()


#FMFFM: Carteras de Inversión de Fondos Mutuos
def get_df_mutual_funds_investment_portfolio(process_date, portfolio_type):

    '''
    param process_date: String referente a la fecha sobre la cual se desea consultar la informacion de las carteras de fondos de inversion. 
    El envío de la cartola es mensual, por tanto, el formato debe ser 'yyyymm'
    param portafolio_type: String referente al tipo de cartola. Valores aceptados: naci (cartera nacional), extr (cartera extranjera), futu (cartera futuros),
    opci (cartera opciones), opla (cartera opciones lanzadas).

    Esta funcion retorna un dataframe con los siguientes datos de las carteras de inversion de fondos mutuos:

    NUMERO|PERIODO|RUN_FONDONOMBRE_FONDO|NOMBRE_CORTO|RUT_ADMINISTRADORA|NOMBRE_ADMINISTRADORA|MONEDA|TIPO_FONDO|NEMOTECNICO|RUT_EMISOR|DV_EMISOR|COD_PAIS|
    TIPO_INSTRUMENTO|FECHA_VENCIMIENTO|SITUACION_INSTR|CLASIF_RIESGO|COD_GRUPO_EMPR|CANTIDAD_UNIDADES|TIPO_UNIDADES|TIR|VALOR_PAR|VALOR_RELEVANTE|CODIGO_VALORIZACION|
    BASE_TASA|TIPO_INTERES|VALORIZACION_CIERRE|COD_MONEDA_LIQ|COD_PAIS_TRANS|PORC_CAPITAL|PORC_TOT_EMI|PORC_TOT_ACT

    '''
    API_URL = f'/sitio/api/fmffm/consulta_cartera/{process_date}/{portfolio_type}/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_mutual_fund_investment_portfolio = pd.DataFrame(json_data['Data'])
        return df_mutual_fund_investment_portfolio
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api, volviendo a intentar')
        get_df_mutual_funds_investment_portfolio(process_date, portfolio_type)
    

#FIFFI: Carteras de Inversión de Fondos de Inversión
def get_df_investment_funds_investment_portfolio(process_date, portfolio_type):
    '''
    param process_date: String con la Fecha sobre la cual se desea consultar la información de Cartola Diaria de Fondos de Inversión,
    el envío de la cartola es mensual, por tanto, el formato debe ser 'yyyymm'.
    param portfolio_type: Tipo de cartola, los valores aceptados son:
        naci: Cartera Nacional
        extr: Cartera Extranjera
        futu: Cartera Futuros
        opci: Cartera Opciones
        brai: Cartera Bienes raíces
        mpar: Cartera Método de Participación
        opla: Cartera de operaciones lanzadas
        vrc_crv: Cartera de Operaciones de venta con compromiso de retrocompra (vrc) y de compra con compromiso de retroventa (crv)
    Esta funcion devuelve un dataframe que contiene la informacion de las carteras de inversion de los fondos de inversion:
    NUMERO|PERIODO|RUN_FONDO|RAZON_SOCIAL|NOMBRE_CORTO|RUT_ADMIN|NOMBRE_ADMIN|RESCATABLE|FI_CLASIF_INST_EST_SIT_FINFI_NEMOTECNICO_INSTRUMENTO|FI_RUT_EMISOR|FI_CODIGO_PAIS_EMISOR|
    FI_TIPO_INSTRUMENTO|FI_FECHA_VENCIMIENTO|FI_SIT_INSTRUMENTO|FI_CLASIF_RIESGO|FI_GRUPO_EMPRESARIAL|FI_CANT_UNIDADES|FI_TIPO_UNIDADES|FI_TIR_VAL_PAR_PRECIO|FI_COD_VALORIZACION|
    FI_BASE_TASA|FI_TIPO_INTERES|FI_VALORIZACION_CIERRE|FI_COD_MON_LIQUIDACION|FI_COD_PAIS_TRANSACCION|FI_PORC_CAPITAL_EMISOR|FI_PORC_TOT_ACTIVO_EMISOR|FI_PORC_TOT_ACTIVO_FONDO|
    '''
    if (len(process_date)>6):
        process_date = process_date[:6]
    API_URL = f'/sitio/api/fiffi/consulta_cartera/{process_date}/{portfolio_type}/json'
    client.request('GET',API_URL, headers=headers)
    response = client.getresponse()
    data_http = response.read()
    json_data = json.loads(data_http)
    if json_data['Status']['Code'] == 200:
        df_investment_fund_investment_portfolio = pd.DataFrame(json_data['Data'])
        return df_investment_fund_investment_portfolio
    else:
        print(f'Error: {json_data["Status"]["Code"]}, no se pudo conectar a la api, volviendo a intentar')
        get_df_investment_funds_investment_portfolio(process_date, portfolio_type)
    
if __name__=='__main__':
    get_df_daily_mutual_funds_statement('20210531')