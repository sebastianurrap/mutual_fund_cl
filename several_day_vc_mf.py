from library_mutual_fund import *
from datetime import datetime, timedelta
import pandas as pd
import time

#the days to download the values ​​of mutual funds are defined
start_date='20220301'
end_date='20220304'

start_date=datetime.strptime(start_date, '%Y%m%d')
end_date=datetime.strptime(end_date, '%Y%m%d')

day_count = (end_date - start_date).days + 1 

for single_date in [d for d in (start_date + timedelta(n) for n in range(day_count)) if d <= end_date]:
    start = time.time()
    single_date=datetime.strftime(single_date, '%Y%m%d')
    time.sleep(15)
    try:
        tdcargatdvcffmm(single_date)
        print("Se cargó exitosamente los valores cuota en tdvcffmm para el día: ", single_date)
    except:
        try:
            time.sleep(10)
            tdcargatdvcffmm(single_date)
            print("Se cargó exitosamente los valores cuota en tdvcffmm para el día: ", single_date)
        except:
            print("Hubo un problema con la descarga para el dia ", single_date)
    end = time.time()
    print("tiempo: ",end - start)